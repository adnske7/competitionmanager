﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CompetMan
{
    class Program
    {
        static void Main(string[] args)
        {
            StartMethod();
        }
        public static void StartMethod()
        {
            Console.WriteLine("Controls: addteam, addfighter, addcoach, addskill, assignskill or get to get information about the fighters");
            string input = Console.ReadLine();

            if (input.ToLower().Replace(" ", "") == "addfighter")
            {
                Console.WriteLine("Name:");
                string name = Console.ReadLine();
                Console.WriteLine("Weight:");
                int weight = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Reach:");
                int reach = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Knockouts:");
                int knockOuts = Int32.Parse(Console.ReadLine());
                Console.WriteLine("TeamId:");
                int teamId = Int32.Parse(Console.ReadLine());
                Competitor comp = new Competitor(name, weight, reach, knockOuts, teamId);
                using (CompetManDbContext abc = new CompetManDbContext()) {
                    abc.Competitors.Add(comp);
                    abc.SaveChanges();
                }
                StartMethod();
            }
            if (input.ToLower().Replace(" ", "") == "addteam") {
                Console.WriteLine("Name:");
                string name = Console.ReadLine();
                Team team1 = new Team(name);
                using (CompetManDbContext ghi = new CompetManDbContext())
                {
                    ghi.Teams.Add(team1);
                    ghi.SaveChanges();
                }
                StartMethod();
            }

            if (input.ToLower().Replace(" ", "") == "addcoach")
            {
                Console.WriteLine("Name: ");
                string name = Console.ReadLine();
                Console.WriteLine("Age: ");
                int age = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Is coach for: ");
                int competitorId = Int32.Parse(Console.ReadLine());
                Coach coach1 = new Coach(name, age, competitorId);
                using (CompetManDbContext def = new CompetManDbContext())
                {
                    def.Coaches.Add(coach1);
                    def.SaveChanges();
                }
                StartMethod();
            }
            if (input.ToLower().Replace(" ", "") == "addskill")
            {
                Console.WriteLine("Description:");
                string description = Console.ReadLine();
                Skill skill1 = new Skill(description);
                using (CompetManDbContext jkl = new CompetManDbContext())
                {
                    jkl.Skills.Add(skill1);
                    jkl.SaveChanges();
                }
                StartMethod();
            }
            if(input.ToLower().Replace(" ","")== "assignskill")
            {
                Console.WriteLine("What is the Id of the fighter you want to assign a skill to?");
                int competitorId = Int32.Parse(Console.ReadLine());
                Console.WriteLine("What is the Id of the skill you want to assign?");
                int skillId = Int32.Parse(Console.ReadLine());
                CompetitorSkill competitorSkill1 = new CompetitorSkill() { CompetitorId = competitorId, SkillId = skillId };
                using (CompetManDbContext bca = new CompetManDbContext())
                {
                    bca.CompetitorSkills.Add(competitorSkill1);
                    bca.SaveChanges();
                }
                StartMethod();
            }

            if (input.ToLower() == "get")
            {
                using (CompetManDbContext abc = new CompetManDbContext())
                {
                    List<Competitor> allFighters = abc.Competitors.ToList();
                    foreach (Competitor fighter in allFighters)
                    {
                        Console.WriteLine("Name: " + fighter.Name + ", Weight in kg: " + fighter.Weight + ", Reach in cm: " + fighter.Reach + ", Knockouts: " + fighter.KnockOuts);
                    }
                    
                }
                StartMethod();
            }

            else {
                Console.WriteLine("Invalid input, try again");
                StartMethod();
            }
           
        }

    }
}
