﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetMan
{
    public class CompetitorSkill
    {
        public Skill Skill {get; set;}
        public int SkillId {get; set;}
        public Competitor Competitor { get; set; }
        public int CompetitorId {get; set;}
        
        
    }
}
