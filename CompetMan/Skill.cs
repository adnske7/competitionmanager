﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetMan
{
    public class Skill
    {
        public int Id { get; set; }
        public string Description { get; set; }
        

        public Skill(string description)
        {
            this.Description = description;
        }


    }
    
}
