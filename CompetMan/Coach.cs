﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetMan
{
    public class Coach
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int CompetitorId { get; set; }
        public Competitor Competitor { get; set; }


        public Coach(string name, int age, int competitorId)
        {
            this.Name = name;
            this.Age = age;
            this.CompetitorId = competitorId;
        }
    }

}
