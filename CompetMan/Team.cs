﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetMan
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Competitor> Competitors { get; set; }

        public Team(string name)
        {
            this.Name = name;
        }
    }
}
