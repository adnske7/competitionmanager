﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompetMan.Migrations
{
    public partial class mig6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_CompetitorSkill_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors");

            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorSkill_Competitors_CompetitorId",
                table: "CompetitorSkill");

            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorSkill_Skills_SkillId",
                table: "CompetitorSkill");

            migrationBuilder.DropForeignKey(
                name: "FK_Skills_CompetitorSkill_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Skills");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompetitorSkill",
                table: "CompetitorSkill");

            migrationBuilder.RenameTable(
                name: "CompetitorSkill",
                newName: "CompetitorSkills");

            migrationBuilder.RenameIndex(
                name: "IX_CompetitorSkill_SkillId",
                table: "CompetitorSkills",
                newName: "IX_CompetitorSkills_SkillId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompetitorSkills",
                table: "CompetitorSkills",
                columns: new[] { "CompetitorId", "SkillId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_CompetitorSkills_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" },
                principalTable: "CompetitorSkills",
                principalColumns: new[] { "CompetitorId", "SkillId" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorSkills_Competitors_CompetitorId",
                table: "CompetitorSkills",
                column: "CompetitorId",
                principalTable: "Competitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorSkills_Skills_SkillId",
                table: "CompetitorSkills",
                column: "SkillId",
                principalTable: "Skills",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Skills_CompetitorSkills_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Skills",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" },
                principalTable: "CompetitorSkills",
                principalColumns: new[] { "CompetitorId", "SkillId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_CompetitorSkills_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors");

            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorSkills_Competitors_CompetitorId",
                table: "CompetitorSkills");

            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorSkills_Skills_SkillId",
                table: "CompetitorSkills");

            migrationBuilder.DropForeignKey(
                name: "FK_Skills_CompetitorSkills_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Skills");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompetitorSkills",
                table: "CompetitorSkills");

            migrationBuilder.RenameTable(
                name: "CompetitorSkills",
                newName: "CompetitorSkill");

            migrationBuilder.RenameIndex(
                name: "IX_CompetitorSkills_SkillId",
                table: "CompetitorSkill",
                newName: "IX_CompetitorSkill_SkillId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompetitorSkill",
                table: "CompetitorSkill",
                columns: new[] { "CompetitorId", "SkillId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_CompetitorSkill_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" },
                principalTable: "CompetitorSkill",
                principalColumns: new[] { "CompetitorId", "SkillId" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorSkill_Competitors_CompetitorId",
                table: "CompetitorSkill",
                column: "CompetitorId",
                principalTable: "Competitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorSkill_Skills_SkillId",
                table: "CompetitorSkill",
                column: "SkillId",
                principalTable: "Skills",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Skills_CompetitorSkill_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Skills",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" },
                principalTable: "CompetitorSkill",
                principalColumns: new[] { "CompetitorId", "SkillId" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
