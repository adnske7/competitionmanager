﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompetMan.Migrations
{
    public partial class mig7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_CompetitorSkills_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors");

            migrationBuilder.DropForeignKey(
                name: "FK_Skills_CompetitorSkills_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Skills");

            migrationBuilder.DropIndex(
                name: "IX_Skills_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Skills");

            migrationBuilder.DropIndex(
                name: "IX_Competitors_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors");

            migrationBuilder.DropColumn(
                name: "CompetitorSkillCompetitorId",
                table: "Skills");

            migrationBuilder.DropColumn(
                name: "CompetitorSkillSkillId",
                table: "Skills");

            migrationBuilder.DropColumn(
                name: "CompetitorSkillCompetitorId",
                table: "Competitors");

            migrationBuilder.DropColumn(
                name: "CompetitorSkillSkillId",
                table: "Competitors");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CompetitorSkillCompetitorId",
                table: "Skills",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompetitorSkillSkillId",
                table: "Skills",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompetitorSkillCompetitorId",
                table: "Competitors",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompetitorSkillSkillId",
                table: "Competitors",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Skills_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Skills",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" });

            migrationBuilder.CreateIndex(
                name: "IX_Competitors_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_CompetitorSkills_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" },
                principalTable: "CompetitorSkills",
                principalColumns: new[] { "CompetitorId", "SkillId" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Skills_CompetitorSkills_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Skills",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" },
                principalTable: "CompetitorSkills",
                principalColumns: new[] { "CompetitorId", "SkillId" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
