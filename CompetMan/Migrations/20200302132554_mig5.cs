﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompetMan.Migrations
{
    public partial class mig5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CompetitorSkillCompetitorId",
                table: "Competitors",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompetitorSkillSkillId",
                table: "Competitors",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(nullable: true),
                    CompetitorSkillCompetitorId = table.Column<int>(nullable: true),
                    CompetitorSkillSkillId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CompetitorSkill",
                columns: table => new
                {
                    SkillId = table.Column<int>(nullable: false),
                    CompetitorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetitorSkill", x => new { x.CompetitorId, x.SkillId });
                    table.ForeignKey(
                        name: "FK_CompetitorSkill_Competitors_CompetitorId",
                        column: x => x.CompetitorId,
                        principalTable: "Competitors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompetitorSkill_Skills_SkillId",
                        column: x => x.SkillId,
                        principalTable: "Skills",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Competitors_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" });

            migrationBuilder.CreateIndex(
                name: "IX_CompetitorSkill_SkillId",
                table: "CompetitorSkill",
                column: "SkillId");

            migrationBuilder.CreateIndex(
                name: "IX_Skills_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Skills",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_CompetitorSkill_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" },
                principalTable: "CompetitorSkill",
                principalColumns: new[] { "CompetitorId", "SkillId" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Skills_CompetitorSkill_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Skills",
                columns: new[] { "CompetitorSkillCompetitorId", "CompetitorSkillSkillId" },
                principalTable: "CompetitorSkill",
                principalColumns: new[] { "CompetitorId", "SkillId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_CompetitorSkill_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors");

            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorSkill_Skills_SkillId",
                table: "CompetitorSkill");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropTable(
                name: "CompetitorSkill");

            migrationBuilder.DropIndex(
                name: "IX_Competitors_CompetitorSkillCompetitorId_CompetitorSkillSkillId",
                table: "Competitors");

            migrationBuilder.DropColumn(
                name: "CompetitorSkillCompetitorId",
                table: "Competitors");

            migrationBuilder.DropColumn(
                name: "CompetitorSkillSkillId",
                table: "Competitors");
        }
    }
}
