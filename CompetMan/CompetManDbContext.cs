﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace CompetMan
{
    public class CompetManDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=PC7397\SQLEXPRESS; Database=Competition; Trusted_Connection=true");
        }
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<CompetitorSkill> CompetitorSkills { get; set; } 
    protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompetitorSkill>().HasKey(pq => new { pq.CompetitorId, pq.SkillId });
        }
    
    }

}
