﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetMan
{
    public class Competitor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Weight { get; set; }
        public int Reach { get; set; }
        public int KnockOuts { get; set; }
        public Coach Coach { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
       

        public Competitor(string name, int weight, int reach, int knockOuts, int teamId)
        {
            this.Name = name;
            this.Weight = weight;
            this.Reach = reach;
            this.KnockOuts = knockOuts;
            this.TeamId = teamId;
        }

    }
}
