# Competition Manager
Program that allows you to create fighters, coaches, teams and skills. Where it is a one to one realtionship between coach and fighter, a many to one between team and fighter, and a many to many between fighter and skill.


## Getting Started
Open the folder in Visual studio, and run the program.cs file there. When adding to an empty database: Team has to be added first, then fighters.


## Author
* **Ådne Skeie** - [adnske7](https://gitlab.com/adnske7)
